# What is it

nw2 is a shared library, which wraps all glibc read/write functions and counts the number of bytes going through sockets. All network IO function calls and their byte count is written to a log file

There are two variants: nw2.so inefficiently opens and closes its log file a lot, but is completely standalone. nw2_msg.so uses an external daemon to collect log messages, and sends information over the POSIX message queues system of the kernel. See the run_test*.sh scripts for examples on how to use.

# Compiling

```bash
make # should need no dependencies at all
```

# Possibility 1: Executing with nw2.so

An example can be found in run_test_wget_old.sh

```bash
export NW2_LOGFILE=NW2.log # optionally set log file location, defaults to "NW2.log"

LD_PRELOAD=/path/to/nw2.so ./myProgram
```

# Possibility 2: Executing with nw2_msg.so

nw2_msg.so needs a running nw2_daemon to collect log information, and nw2_finish to shut the daemon down.
The environment variable NW2_MQNAME must be set and identifies the job. All networking activity of processes where this env variable is set will end up at the same daemon. For multiple concurrent jobs on the same machine, use unique NW2_MQNAMES to not mix their logs.


```bash
export NW2_MQNAME=/test123   # the message queue name must begin with a / on Linux systems
./nw2_daemon > NW2.log &  # start daemon in the backgroudn
LD_PRELOAD=/path/to/nw2_msg.so ./myProgram
./nw_finish   # notifies the daemon to stop after processing its message queue
```



# Analysing the log files

```bash

./nw2_format_log.py NW2.log > NW2.cummulative.log # sums up into this format: timestamp	netread	netwrite

# plot with gnuplot
gnuplot
gnuplot> set ylabel "data [MB]"
gnuplot> plot "NW2.cummulative.log" using 1:($2/1024**2) title 'read', "" using 1:($3/1024**2) title "write"
```

# Capabilities and shortcomings

Seems to work with wget, scp, xrdcp and Reco_tf.py without crashing while reporting plausible sounding numbers. Works for all processes spawned by first process.

Is possibly threadsafe now. Log files are a bit big. nw2.so causes a lot of unneccecary IO, use nw2_msg.so instead.