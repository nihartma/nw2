#!/bin/bash

logfile=$1
shift 1

# close message queue when script exits
function finish {
    nw2_finish
}
trap finish EXIT

export MYUUID=$(uuidgen)
export NW2_MQNAME=/${MYUUID}
echo "Message queue name for this job is " $NW2_MQNAME
nw2_daemon | nw2_format_log.py > $logfile &

export LD_PRELOAD=${HOME}/ld_preload/nw2_msg.so:${LD_PRELOAD}
echo "Preloading ${LD_PRELOAD} to intercept networking calls"

# execute remaining command line
$@
