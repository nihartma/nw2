#define _GNU_SOURCE  // need for RTLD_NEXT


#include <dlfcn.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <mqueue.h>

#include <errno.h>
#include "nw2_msg.h"

extern char *program_invocation_name;
extern char *program_invocation_short_name;

static size_t recv_count = 0;
static size_t send_count = 0;

static pid_t pid = -1;

static ssize_t (*real_read)(int fildes, void *buf, size_t nbyte) = NULL;
static ssize_t (*real_pread)(int fildes, void *buf, size_t nbyte, off_t offset) = NULL;
static ssize_t (*real_recv)(int socket, void *buffer, size_t length, int flags) = NULL;
static ssize_t (*real_recvfrom)(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) = NULL;
static ssize_t (*real_recvmsg)(int socket, struct msghdr *message, int flags) = NULL;
static ssize_t (*real_write)(int fildes, const void *buf, size_t nbyte) = NULL;
static ssize_t (*real_pwrite)(int fildes, const void *buf, size_t nbyte, off_t offset) = NULL;
static ssize_t (*real_send)(int socket, const void *buffer, size_t length, int flags) = NULL;
static ssize_t (*real_sendto)(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) = NULL;
static ssize_t (*real_sendmsg)(int socket, const struct msghdr *message, int flags) = NULL;


static void log_event(enum nw2_call call, ssize_t nbytes) {
    time_t time_now = time(NULL);
    pid_t pid = getpid();
  
    static mqd_t mq = -1;
    static char *mqname = NULL;
    if (!mqname) {
      mqname = getenv("NW2_MQNAME");
    }
    
    if (!mqname){ return; }
    
    
    struct nw2_message msg;
  
    if (mq < 0) {    
      struct mq_attr attr;
      attr.mq_maxmsg = 10;
      attr.mq_msgsize = sizeof(msg);
      
      mq = mq_open(mqname, O_CREAT | O_RDWR, 0644, &attr);
    }
    
    msg.call = call;
    msg.nbytes = nbytes;
    msg.pid = pid;
    msg.time = time_now;
    
    mq_send(mq, (char *) &msg, sizeof(msg), 0);

}



int is_socket(int fildes) {
    struct stat statbuf;
    fstat(fildes, &statbuf);
    return S_ISSOCK(statbuf.st_mode);
}


/*
void __attribute__ ((constructor)) my_init(void) {
    pid = getpid();
    char string_buf[102400];

    size_t recv_count_old = recv_count;
    recv_count = 0;

    size_t send_count_old = send_count;
    send_count = 0;

    sprintf ( string_buf, "nw2-%d-%s.txt", pid, program_invocation_short_name );
    FILE *outfile = fopen(string_buf, "a");
    //fprintf(outfile, "%d %s %s <attribute init> \n", pid, program_invocation_name, program_invocation_short_name);
    fprintf(outfile, "%d <init total socket read> %ld\n <was total socket read> %ld\n", pid,  recv_count, recv_count_old);
    fprintf(outfile, "%d <init total socket write> %ld\n <was total socket write> %ld\n", pid,  send_count, send_count_old);









//     if (!real_read) fprintf(outfile, "%d <init no real_read> \n", pid);
//     if (!real_pread) fprintf(outfile, "%d <init no preal_read> \n", pid);
//     if (!real_recv) fprintf(outfile, "%d <init no real_recv> \n", pid);
//     if (!real_recvfrom) fprintf(outfile, "%d <init no real_recvfrom> \n", pid);
//     if (!real_recvmsg) fprintf(outfile, "%d <init no real_revcmsg> \n", pid);
//     if (!real_write) fprintf(outfile, "%d <init no real_write> \n", pid);
//     if (!real_pwrite) fprintf(outfile, "%d <init no real_pwrite> \n", pid);
//     if (!real_send) fprintf(outfile, "%d <init no real_send> \n", pid);
//     if (!real_sendto) fprintf(outfile, "%d <init no real_sendto> \n", pid);
//     if (!real_sendmsg) fprintf(outfile, "%d <init no real_sendmsg> \n", pid);


    fclose(outfile);
}



void __attribute__ ((destructor)) my_fini(void) {
    char string_buf[1024];

    sprintf ( string_buf, "nw2-%d-%s.txt", pid, program_invocation_short_name );
    FILE *outfile = fopen(string_buf, "a");
    //fprintf(outfile, "%d <attribute fini> \n", pid);
    fprintf(outfile, "%d <fini total socket read> %ld\n", pid,  recv_count);
    fprintf(outfile, "%d <fini total socket write> %ld\n", pid,  send_count);

    fclose(outfile);
}

*/
/*
void myexit() {
    ssize_t pid = getpid();
    char string_buf[1024];

    sprintf ( string_buf, "%ld-%s.txt", pid, program_invocation_short_name );
    FILE *outfile = fopen(string_buf, "a");
    fprintf(outfile, "%ld <exit> \n", pid);
    fprintf(outfile, "%ld <total socket read> %ld\n", pid,  recv_count);
    fclose(outfile);
}*/

/*
void register_exit() {
  static int done = 0;
  if (!done) {
    atexit(myexit);
    done = 1;
  }
  return;
}*/

ssize_t read(int fildes, void *buf, size_t nbyte) {
  if (!real_read) {
    real_read = dlsym(RTLD_NEXT, "read");
  }
    ssize_t ret = real_read(fildes, buf, nbyte);

    //fprintf(outfile, "%ld <read %s> %ld\n", pid, bla, ret);

    if(is_socket(fildes)) {
	recv_count += ret;
	log_event(nw2_read, ret);
    }

    return ret;
}



ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset) {
  if (!real_pread) {
    real_pread = dlsym(RTLD_NEXT, "pread");
  }
    ssize_t ret = real_pread(fildes, buf, nbyte, offset);
    if (is_socket(fildes)) {
	recv_count += ret;
	log_event(nw2_pread, ret);
    }
    return ret;

}


ssize_t recv(int socket, void *buffer, size_t length, int flags) {
  if (!real_recv) {
    real_recv = dlsym(RTLD_NEXT, "recv");
  }
    ssize_t ret = real_recv(socket, buffer, length, flags);
    recv_count += ret;
    log_event(nw2_recv, ret);
    return ret;
}


ssize_t recvfrom(int socket, void * buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
  if (!real_recvfrom) {
    real_recvfrom = dlsym(RTLD_NEXT, "recvfrom");
  }
    ssize_t ret = real_recvfrom(socket, buffer, length, flags, address, address_len);
    recv_count += ret;
    log_event(nw2_recvfrom, ret);
    return ret;

}

ssize_t recvmsg(int socket, struct msghdr *message, int flags) {
  if (!real_recvmsg) {
    real_recvmsg = dlsym(RTLD_NEXT, "recvmsg");
  }
    ssize_t ret = real_recvmsg(socket, message, flags);
    recv_count += ret;
    log_event(nw2_recvmsg, ret);
    return ret;
}
//
ssize_t write(int fildes, const void *buf, size_t nbyte) {
  if (!real_write) {
    real_write = dlsym(RTLD_NEXT, "write");
  }
    ssize_t ret = real_write(fildes, buf, nbyte);
    if(is_socket(fildes)) {
	send_count += ret;
	log_event(nw2_write, ret);
    }
    return ret;
}

ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset) {
  if (!real_pwrite) {
    real_pwrite = dlsym(RTLD_NEXT, "pwrite");
  }
    ssize_t ret = real_pwrite(fildes, buf, nbyte, offset);
    if(is_socket(fildes)) {
	send_count += ret;
	log_event(nw2_pread, ret);
    }
    return ret;
}


ssize_t send(int socket, const void *buffer, size_t length, int flags) {
  if (!real_send) {
    real_send = dlsym(RTLD_NEXT, "send");
  }
    ssize_t ret = real_send(socket, buffer, length, flags);
    send_count += ret;
    log_event(nw2_send, ret);
    return ret;
}


ssize_t sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
  if (!real_sendto) {
    real_sendto = dlsym(RTLD_NEXT, "sendto");
  }
    ssize_t ret = real_sendto(socket, message, length, flags, dest_addr, dest_len);
    send_count += ret;
    log_event(nw2_sendto, ret);
    return ret;
}

ssize_t sendmsg(int socket, const struct msghdr *message, int flags) {
  if (!real_sendmsg) {
    real_sendmsg = dlsym(RTLD_NEXT, "sendmsg");
  }
    ssize_t ret = real_sendmsg(socket, message, flags);
    send_count += ret;
    log_event(nw2_sendmsg, ret);
    return ret;

}


