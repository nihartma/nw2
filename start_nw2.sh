# this script is meant to be sourced

if [[ -z "$1" ]]
then
    echo "Need to pass a logfile!"
    return 1
fi

logfile=$1

echo "Writing to logfile $logfile"

export MYUUID=$(uuidgen)
export NW2_MQNAME=/${MYUUID}
echo "Message queue name for this job is " $NW2_MQNAME
nw2_daemon | nw2_format_log.py > $logfile &

export LD_PRELOAD=${HOME}/ld_preload/nw2_msg.so:${LD_PRELOAD}
echo "Preloading ${LD_PRELOAD} to intercept networking calls"

echo "Run nw2_finish to stop daemon"
