all: nw2.so nw2_msg.so nw2_daemon nw2_finish

nw2.so: nw2.c
	gcc -shared -fPIC -O2  nw2.c  -o nw2.so -ldl

nw2_msg.so: nw2_msg.c nw2_msg.h
	gcc -shared -fPIC -O2  nw2_msg.c  -o nw2_msg.so -ldl -lrt -g

nw2_daemon: nw2_daemon.c nw2_msg.h
	gcc -O2  nw2_daemon.c  -o nw2_daemon -ldl -lrt -g 


nw2_finish: nw2_finish.c nw2_msg.h
	gcc -O2  nw2_finish.c  -o nw2_finish -ldl -lrt -g 


.PHONY: clean all

clean:
	rm -f nw2.so nw2_msg.so nw2_daemon.so nw2_finish

