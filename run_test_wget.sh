#!/bin/bash
function finish {
    ./nw2_finish
}
trap finish EXIT

export MYUUID=$(uuid)
export NW2_MQNAME=/${MYUUID}
echo "Message queue name for this job is " $NW2_MQNAME
./nw2_daemon > $MYUUID.log &

export LD_PRELOAD=${PWD}/nw2_msg.so
echo "Preloading ${LD_PRELOAD} to intercept networking calls"

wget https://root.cern.ch/download/root_v6.12.06.Linux-ubuntu16-x86_64-gcc5.4.tar.gz -O - > /dev/null


