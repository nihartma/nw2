#!/usr/bin/env python

from __future__ import print_function
import sys

funcnames_read = set(['read', 'pread', 'recv', 'recvfrom', 'recvmsg'])
funcnames_write = set(['write', 'pwrite', 'send', 'sendto', 'sendmsg'])

print("#timestamp\tnetread\tnetwrite")

prev_timestamp = None
nbytes_read = 0
nbytes_write = 0
#with open(sys.argv[1]) as logfile:
if len(sys.argv) > 1:
    logfile = open(sys.argv[1])
else:
    logfile = sys.stdin

for line in logfile:
    if line.startswith("#") : continue

    timestamp, pid, funcname, nbytes = line.strip().split('\t')
    nbytes = int(nbytes)
    if nbytes < 0:
        continue

    if prev_timestamp != timestamp:
        if prev_timestamp is not None:
            print("{}\t{}\t{}".format(prev_timestamp, nbytes_read, nbytes_write))
        nbytes_read = 0
        nbytes_write = 0
    prev_timestamp = timestamp

    #print(timestamp, pid, funcname, nbytes)
    if funcname in funcnames_read:
        nbytes_read += nbytes

    elif funcname in funcnames_write:
        nbytes_write += nbytes

    else:
        raise KeyError("Unknown function '{}'.format(funcname)")

# write last entry
if prev_timestamp is not None:
    print("{}\t{}\t{}".format(timestamp, nbytes_read, nbytes_write))
